Come usare questo bot?

1. Ottenere un token personale da BotFather. Chiedi a Google come.
2. Rinominare il file **secret_sample.py** in **secret.py**
3. Inserire all'interno del file **secret.py** il token ottenuto nel punto 1
   nel punto indicato.
4. Lanciare (o creare un virtualenv prima) il comando: 
> `python3 -r install requirements.txt`
5. Lanciare il BOT con:
> `python3 main.py`


Enjoy
