from telegram import ParseMode
from main import divider, show_grocery_list, rent_payer, add_to_grocery_list, what_is_missing_at_home, \
    remove_from_grocery_list, help, start, createinlinkekeyboard


def execute_commands(bot, update):
    message = update.callback_query.data
    chat_id = update.effective_chat.id
    inlinekeyboard = createinlinkekeyboard()
    if message == "/start":
        start(bot, update)
        return
    elif message == "/dividi":
        text = "Tieni premuto sulla scritta blu e inserisci il numero\n" \
               "<b>/dividi 4567.89</b>"

    elif message == "/affitto":
        text = rent_payer(bot, update, send=False)

    elif message == "/lista_spesa":
        text = show_grocery_list(bot, update, send=False)

    elif message == "/aggiungi_a_lista":
        text = "Tieni premuto sulla scritta blu e inserisci l'oggetto da aggiungere alla lista\n" \
               "<b>/aggiungi_a_lista latte</b>"

    elif message in what_is_missing_at_home:
        text = remove_from_grocery_list(bot, update, message)
        inlinekeyboard = createinlinkekeyboard(keyboard_oggetti=True)

    elif message == "/rimuovi_da_lista":
        text = "Scegli l'elemento che vuoi rimuovere dalla lista"
        inlinekeyboard = createinlinkekeyboard(keyboard_oggetti=True)

    elif message == "/help":
        text = help(bot, update, send=False)

    else:
        bot.deleteMessage(update.effective_chat.id, update.effective_message.message_id)
        return

    try:
        bot.editMessageText(chat_id=chat_id,
                            message_id=update.callback_query.message.message_id,
                            text=text,
                            reply_markup=inlinekeyboard,
                            parse_mode=ParseMode.HTML)
    except:
        bot.send_message(chat_id=chat_id, text=text,
                         reply_markup=inlinekeyboard,
                         parse_mode=ParseMode.HTML)


def execute_divider(bot, update):
    chat_id = update.effective_chat.id
    args = update.effective_message.text.split(" ")
    try:
        value = float(args[1].replace(",", ".").strip())
        text = divider(value)
    except (ValueError, IndexError):
        text = "Tieni premuto sulla scritta blu e inserisci il numero\n" \
               "<b>/dividi 4567.89</b>"

    bot.send_message(chat_id=chat_id, text=text, parse_mode=ParseMode.HTML)


def execute_add_item(bot, update):
    chat_id = update.effective_chat.id
    args = update.effective_message.text.split(" ")
    try:
        int(args[1].strip())
        text = "Per aggiungere qualcosa alla lista tieni premuto sulla scritta blu e scrivi il nome, ad esempio: \n" \
               "<b>/aggiungi_a_lista formaggio</b>"
    except ValueError:
        text = add_to_grocery_list(args[1].strip())
    except IndexError:
        text = "Per aggiungere qualcosa alla lista tieni premuto sulla scritta blu e scrivi il nome, ad esempio: \n" \
               "<b>/aggiungi_a_lista formaggio</b>"

    bot.send_message(chat_id=chat_id, text=text, parse_mode=ParseMode.HTML)


