
class Lst:
    def __init__(self):
        self.lst = list()

    def __iter__(self):
        return iter(self.lst)

    def __bool__(self):
        return bool(self.lst)

    def insert(self, item_to_buy):
        self.lst.append(item_to_buy)

    def remove(self, item_bought):
        self.lst.remove(item_bought)