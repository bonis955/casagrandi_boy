import grocery
import sqlite3
import locale
import logging  # libreria per debuggare

from execute_command import *
from secret import Hidden
from telegram import ParseMode, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import Updater, CallbackQueryHandler, CommandHandler, MessageHandler, Filters
from datetime import date
from decimal import *

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

# Change output language as Italian
locale.setlocale(locale.LC_TIME, "it_IT")

# Set decimal places after comma
TWOPLACES = Decimal(10) ** -2

# Create class instance for glocery list
what_is_missing_at_home = grocery.Lst()

# Define rommates
ROOMMATES = {
    "Mattia": list(),
    "Massimiliano": list(),
    "Cristiana": list(),
    "Carla": list()
}


def start(bot, update):
    welcome_message = "Ciao, sono il bot di <b>Casa Grandi!</b>\n" \
                      "Scegli un azione da compiere:"
    bot.send_message(chat_id=update.effective_chat.id, text=welcome_message, reply_markup=createinlinkekeyboard(),
                     parse_mode=ParseMode.HTML)


def echo(bot, update):
    bot.send_message(chat_id=update.effective_chat.id, text=update.effective_message.text,
                     reply_markup=createinlinkekeyboard())


def help(bot, update, send=True):
    text = "Ecco la lista dei comandi disponibili\n\n" \
           "<b>Dividi</b> -> Calcola le quote dell'affitto\n" \
           "<b>Affitto</b> -> A chi tocca pagare l'affitto\n" \
           "<b>Lista_spesa</b> -> Quali prodotti mancano in casa?\n" \
           "<b>Aggiungi_a_lista</b> -> Aggiungi un prodotto a lista della spesa\n" \
           "<b>rimuovi_da_lista</b> -> Rimuovi un prodotto da lista spesa\n" \
           "<b>Help</b> -> Rimuovi un prodotto da lista spesa"

    if send:
        bot.send_message(chat_id=update.effective_chat.id, text=text, parse_mode="HTML",
                         reply_markup=createinlinkekeyboard())
        return
    return text


def rent_payer(bot, update, send=True):
    month = date.today().month
    message = "Questo mese deve pagare <b>" + list(ROOMMATES.keys())[month % 3] + "</b>"
    if send:
        bot.send_message(chat_id=update.effective_chat.id, text=message,reply_markup=createinlinkekeyboard())
        return
    return message


def show_grocery_list(bot, update, send=True):
    if what_is_missing_at_home:
        print_list = "Lista della spesa:\n"
        for item in what_is_missing_at_home:
            print_list += "• " + item + "\n"
    else:
        print_list = "A casa non manca nulla! :)\n\nSe vuoi aggiungere qualcosa tieni premuto sulla scritta blu e metti il nome\n" \
                     "\n<b>/aggiungi_a_lista pane</b>"
    if send:
        bot.send_message(chat_id=update.effective_chat.id, text=print_list, reply_markup=createinlinkekeyboard())
        return
    return print_list


def add_to_grocery_list(oggetto):
    # print_me = ""
    if isinstance(oggetto.strip(), str):
        if oggetto in what_is_missing_at_home:
            print_me = str(oggetto) + " è già presente nella lista."
        else:
            what_is_missing_at_home.insert(oggetto)
            print_me = "Ho inserito " + str(oggetto) + " nella lista"
    else:
        print_me = str(oggetto) + " non risulta un valido oggetto da aggiungere alla lista" \
                                  "Tieni premuto sulla scritta blu /aggiungi_a_lista e metti la cosa che vuoi " \
                                  "aggiungere"

    return print_me


def remove_from_grocery_list(bot, update, oggetto):
    what_is_missing_at_home.remove(str(oggetto))
    return "Ho rimosso %s dala lista\n\n%s" % (oggetto, show_grocery_list(bot, update))


def compute_result(rent: float) -> dict:
    remainder = Decimal(rent % 12).quantize(TWOPLACES)
    share = Decimal((rent - (rent % 12)) / 12).quantize(TWOPLACES)
    for person in ROOMMATES.keys():
        ROOMMATES[person].append(Decimal(share + remainder / 4).quantize(TWOPLACES))
        ROOMMATES[person].append(share)
        ROOMMATES[person].append(share)
    return ROOMMATES


def divider(value):
    money_per_person = compute_result(value)
    message = ""
    month = date.today().month
    months_group = list()
    if month % 3 == 0:
        months_group = [month - 2, month - 1, month]
    elif month % 3 == 2:
        months_group = [month - 1, month, month + 1]
    elif month % 3 == 1:
        months_group = [month, month + 1, month + 2]
    months = [date(year=date.today().month, month=x, day=1).strftime("%B") for x in months_group]

    for key, val in money_per_person.items():
        message += f'<b>{key}</b> deve:\n'
        for index, i in enumerate(val):
            message += f'\t\t\t\t{months[index]}: ' + f'{i}' + "\n"
        message += "\n"

    return message


def unknown(bot, update):
    bot.send_message(chat_id=update.effective_chat.id, text="Mi dispiace, non riesco a capire il comando.",
                     reply_markup=createinlinkekeyboard())


def error(bot, update):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', bot, update.error)


def db_start_connection():
    try:
        sqliteConnection = sqlite3.connect('casa_grande_boy_db.db')
        print("Database created and Successfully Connected to SQLite")
        # cursor = sqliteConnection.cursor()
        # sqlite_select_Query = "select sqlite_version();"
        # cursor.execute(sqlite_select_Query)
        # record = cursor.fetchall()
        # print("SQLite Database Version is: ", record)
        # cursor.close()
    except sqlite3.Error as error:
        print("Error while connecting to sqlite", error)
    finally:
        return sqliteConnection


def db_stop_connection(connection):
    if connection:
        connection.close()
        print("The SQLite connection is closed")


def main(database_connection):
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(Hidden.token)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(CallbackQueryHandler(execute_commands))
    dispatcher.add_handler(CommandHandler('help', help))
    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CommandHandler('dividi', execute_divider))
    dispatcher.add_handler(CommandHandler('affitto', rent_payer))
    dispatcher.add_handler(CommandHandler('lista_spesa', show_grocery_list))
    dispatcher.add_handler(CommandHandler('aggiungi_a_lista', execute_add_item))

    # on noncommand i.e message - echo the message on Telegram
    # dispatcher.add_handler(MessageHandler(Filters.text, echo))
    # questo handler deve essere posizionato come ultimo, altrimenti coprirà altre funzionalità
    dispatcher.add_handler(MessageHandler(Filters.command, unknown))

    # log all errors
    dispatcher.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    # Start the database
    my_db_conn = db_start_connection().cursor()

    main(my_db_conn)

    # Stop the database
    db_stop_connection(my_db_conn)


def createinlinkekeyboard(keyboard_oggetti=False):
    if not keyboard_oggetti:
        return InlineKeyboardMarkup(inline_keyboard=[
            [InlineKeyboardButton(text='Dividi', callback_data="/dividi"),
             InlineKeyboardButton(text='Affitto', callback_data="/affitto")],
            [InlineKeyboardButton(text='Lista della spesa', callback_data="/lista_spesa"),
             InlineKeyboardButton(text='Aiuto', callback_data="/help")],
            [InlineKeyboardButton(text='Aggiungi a lista', callback_data="/aggiungi_a_lista"),
             InlineKeyboardButton(text='Rimuovi da lista', callback_data="/rimuovi_da_lista")],
            [InlineKeyboardButton(text='Chiudi', callback_data="/chiudi")],
        ])
    else:
        inlinekeyboardlist = list()
        for oggetto in what_is_missing_at_home:
            inlinekeyboardlist.append([InlineKeyboardButton(text=oggetto.capitalize(),
                                                            callback_data=oggetto)])
        inlinekeyboardlist.append([InlineKeyboardButton(text="Chiudi",
                                                        callback_data="/start")])

        return InlineKeyboardMarkup(inline_keyboard=inlinekeyboardlist)